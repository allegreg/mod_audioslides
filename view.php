<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Resource module version information
 *
 * @package    mod_audioslides
 * @copyright  2019 SILECS    derived from  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/mod/audioslides/lib.php');
require_once($CFG->dirroot.'/mod/audioslides/locallib.php');
require_once($CFG->libdir.'/completionlib.php');

$id       = optional_param('id', 0, PARAM_INT); // Course Module ID
$r        = optional_param('r', 0, PARAM_INT);  // Resource instance ID
$redirect = optional_param('redirect', 0, PARAM_BOOL);
$forceview = optional_param('forceview', 0, PARAM_BOOL);

if ($r) {
    if (!$audioslides = $DB->get_record('audioslides', array('id'=>$r))) {
        audioslides_redirect_if_migrated($r, 0);
        print_error('invalidaccessparameter');
    }
    $cm = get_coursemodule_from_instance('audioslides', $audioslides->id, $audioslides->course, false, MUST_EXIST);

} else {
    if (!$cm = get_coursemodule_from_id('audioslides', $id)) {
        audioslides_redirect_if_migrated(0, $id);
        print_error('invalidcoursemodule');
    }
    $audioslides = $DB->get_record('audioslides', array('id'=>$cm->instance), '*', MUST_EXIST);
}

$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/audioslides:view', $context);

// Completion and trigger events.
audioslides_view($audioslides, $course, $cm, $context);

$PAGE->set_url('/mod/audioslides/view.php', array('id' => $cm->id));

if ($audioslides->tobemigrated) {
    audioslides_print_tobemigrated($audioslides, $cm, $course);
    die;
}

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'mod_audioslides', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
if (count($files) < 1) {
    audioslides_print_filenotfound($audioslides, $cm, $course);
    die;
} else {
    $file = reset($files);
    unset($files);
}

$audioslides->mainfile = $file->get_filename();
$displaytype = audioslides_get_final_display_type($audioslides);
if ($displaytype == RESOURCELIB_DISPLAY_OPEN || $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD) {
    $redirect = true;
}

// Don't redirect teachers, otherwise they can not access course or module settings.
if ($redirect && !course_get_format($course)->has_view_page() &&
        (has_capability('moodle/course:manageactivities', $context) ||
        has_capability('moodle/course:update', context_course::instance($course->id)))) {
    $redirect = false;
}

if ($redirect && !$forceview) {
    // coming from course page or url index page
    // this redirect trick solves caching problems when tracking views ;-)
    $path = '/'.$context->id.'/mod_audioslides/content/'.$audioslides->revision.$file->get_filepath().$file->get_filename();
    $fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
    redirect($fullurl);
}

switch ($displaytype) {
    case RESOURCELIB_DISPLAY_EMBED:
        audioslides_display_embed($audioslides, $cm, $course, $file);
        break;
    case RESOURCELIB_DISPLAY_FRAME:
        audioslides_display_frame($audioslides, $cm, $course, $file);
        break;
    default:
        audioslides_print_workaround($audioslides, $cm, $course, $file);
        break;
}

