<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Resource module admin settings and defaults
 *
 * @package    mod_audioslides
 * @copyright  2019 SILECS    derived from  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    require_once("$CFG->libdir/resourcelib.php");

    $displayoptions = resourcelib_get_displayoptions(array(RESOURCELIB_DISPLAY_AUTO,
                                                           RESOURCELIB_DISPLAY_EMBED,
                                                           RESOURCELIB_DISPLAY_FRAME,
                                                           RESOURCELIB_DISPLAY_DOWNLOAD,
                                                           RESOURCELIB_DISPLAY_OPEN,
                                                           RESOURCELIB_DISPLAY_NEW,
                                                           RESOURCELIB_DISPLAY_POPUP,
                                                          ));
    $defaultdisplayoptions = array(RESOURCELIB_DISPLAY_AUTO,
                                   RESOURCELIB_DISPLAY_EMBED,
                                   RESOURCELIB_DISPLAY_DOWNLOAD,
                                   RESOURCELIB_DISPLAY_OPEN,
                                   RESOURCELIB_DISPLAY_POPUP,
                                  );

    //--- general settings -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_configtext('audioslides/framesize',
        get_string('framesize', 'audioslides'), get_string('configframesize', 'audioslides'), 130, PARAM_INT));
    $settings->add(new admin_setting_configmultiselect('audioslides/displayoptions',
        get_string('displayoptions', 'audioslides'), get_string('configdisplayoptions', 'audioslides'),
        $defaultdisplayoptions, $displayoptions));

    //--- modedit defaults -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_heading('audioslidesmodeditdefaults', get_string('modeditdefaults', 'admin'), get_string('condifmodeditdefaults', 'admin')));

    $settings->add(new admin_setting_configcheckbox('audioslides/printintro',
        get_string('printintro', 'audioslides'), get_string('printintroexplain', 'audioslides'), 1));
    $settings->add(new admin_setting_configselect('audioslides/display',
        get_string('displayselect', 'audioslides'), get_string('displayselectexplain', 'audioslides'), RESOURCELIB_DISPLAY_AUTO,
        $displayoptions));
    $settings->add(new admin_setting_configcheckbox('audioslides/showsize',
        get_string('showsize', 'audioslides'), get_string('showsize_desc', 'audioslides'), 0));
    $settings->add(new admin_setting_configcheckbox('audioslides/showtype',
        get_string('showtype', 'audioslides'), get_string('showtype_desc', 'audioslides'), 0));
    $settings->add(new admin_setting_configcheckbox('audioslides/showdate',
        get_string('showdate', 'audioslides'), get_string('showdate_desc', 'audioslides'), 0));
    $settings->add(new admin_setting_configtext('audioslides/popupwidth',
        get_string('popupwidth', 'audioslides'), get_string('popupwidthexplain', 'audioslides'), 620, PARAM_INT, 7));
    $settings->add(new admin_setting_configtext('audioslides/popupheight',
        get_string('popupheight', 'audioslides'), get_string('popupheightexplain', 'audioslides'), 450, PARAM_INT, 7));
    $options = array('0' => get_string('none'), '1' => get_string('allfiles'), '2' => get_string('htmlfilesonly'));
    $settings->add(new admin_setting_configselect('audioslides/filterfiles',
        get_string('filterfiles', 'audioslides'), get_string('filterfilesexplain', 'audioslides'), 0, $options));
}
